function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Pickup units and transport them to the base",
		parameterDefs = {
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "radius",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end


function Run(self, units, parameter)
	
	if (Spring.IsGameOver()==false)then
	
		local myTeamID = Spring.GetMyTeamID()
		local allMyUnits = Spring.GetTeamUnits(myTeamID) --GetSelectedUnits() 
		local counter = #allMyUnits
		
		local radiusTU = parameter.radius
		local pos = parameter.position -- Vec3
		
		local passengers = {}
		local ind = 1
		local pasNum = 0
		
		local isTransporting = nil
		local unitPos = nil --position of the unit on the map
		local distToBase = 0 --distance from the unit to the base
		
		
		for i = 1, counter do
			isTransporting = Spring.GetUnitIsTransporting(allMyUnits[i]) 
			--UnitDefs[Spring.GetUnitDefID(allMyUnits[i])].transportSize
			
			if (isTransporting~=nil) --only for transport
			then  
				
				unitPos = Vec3(Spring.GetUnitPosition(allMyUnits[i]))
				distToBase = ((pos.x - unitPos.x)^2+(pos.y - unitPos.y)^2+(pos.z - unitPos.z)^2)^(0.5)
			
				if (#isTransporting>0) --if transporting a unit - go to the defined position
				then
					if (distToBase<radiusTU) then
						
						
						local unitRadius = UnitDefs[Spring.GetUnitDefID(allMyUnits[i])].radius
						--local unitsInRange = 
						local unitCount = #Spring.GetUnitsInCylinder(unitPos.x, unitPos.z, unitRadius+30, myTeamID)
						
						if (unitsInRange~=nil)then 
							unitCount = #unitsInRange
						end
						
						if (unitCount<=2)then
							Spring.GiveOrderToUnit(allMyUnits[i],CMD.UNLOAD_UNITS,{unitPos.x, pos.y, unitPos.z, 40},{})
							--Spring.GiveOrderToUnit(allMyUnits[i],CMD.UNLOAD_UNITS,{pos.x, pos.y, pos.z, 0},{}) 					
						else 
							local toX = pos.x + math.random(0-radiusTU,radiusTU)
							local toZ = pos.y + math.random(0-radiusTU,radiusTU)
							Spring.GiveOrderToUnit(allMyUnits[i],10, {toX,pos.y,toZ},{""})
						end
						
						
					else
						Spring.GiveOrderToUnit(allMyUnits[i],10, {pos.x,pos.y,pos.z},{""})
					end
				else  --if empty - go load some unit
					for j = 1, #allMyUnits do
						local cantBeTransported = UnitDefs[Spring.GetUnitDefID(allMyUnits[j])].isGroundUnit
						if (cantBeTransported == true) then 
							local unitPos = Vec3(Spring.GetUnitPosition(allMyUnits[j]))
							local distToBase = ((pos.x - unitPos.x)^2+(pos.y - unitPos.y)^2+(pos.z - unitPos.z)^2)^(0.5)
							if (distToBase>radiusTU or distToBase>1500) then -- only units that are further than radius to the given position are accepted
								passengers[ind] = allMyUnits[j]
								ind = ind+1
							end
						end
					end
					pasNum = #passengers
					if (passengers~=nil) then
						local pas = passengers[1]
						local pasLoc = Vec3(Spring.GetUnitPosition(passengers[1]))			
						local closestDist = ((pasLoc.x - unitPos.x)^2+(pasLoc.y - unitPos.y)^2+(pasLoc.z - unitPos.z)^2)^(0.5)  
						
						local enemyTeams = Sensors.core.EnemyTeamIDs()	
						local alliesNear = #Spring.GetUnitsInCylinder(pasLoc.x, pasLoc.z, 200, myTeamID)
						
						for k=1, #enemyTeams do
							local teamID = enemyTeams[k]
							local enemies =  Spring.GetUnitsInCylinder(pasLoc.x, pasLoc.z, 3000, teamID)
							if (#enemies>0)then
								closestDist = closestDist+5000
								if (alliesNear>0)then
									closestDist = closestDist-500*alliesNear
								end
								break
							end
						end
		
						if (pasNum>1) then
							for j = 2, pasNum do --find the closest unit to pick up
								pasLoc = Vec3(Spring.GetUnitPosition(passengers[j]))
								local tempDist = ((pasLoc.x - unitPos.x)^2+(pasLoc.y - unitPos.y)^2+(pasLoc.z - unitPos.z)^2)^(0.5)
								alliesNear = #Spring.GetUnitsInCylinder(pasLoc.x, pasLoc.z, 300, myTeamID)
								for k=1, #enemyTeams do
									local teamID = enemyTeams[k]
									local enemies =  Spring.GetUnitsInCylinder(pasLoc.x, pasLoc.z, 3000, teamID)
									if (#enemies>0)then
										tempDist = tempDist+5000
										if (alliesNear>0)then
											tempDist = tempDist-500*alliesNear
										end
										break
									end
								end
									
								if (tempDist<closestDist) then
									closestDist = tempDist
									pas = passengers[j]
								end
							end
						end
						pasLoc = Vec3(Spring.GetUnitPosition(pas))
						Spring.GiveOrderToUnit(allMyUnits[i],CMD.LOAD_UNITS,{pasLoc.x, pasLoc.y, pasLoc.z, 1000},{})
					end
				end
			else
			--other units' orders
			end
			
		end
		
		
		local allTrans = {}
		ind = 1
		
		for i = 1, #allMyUnits do
			local isTrans = UnitDefs[Spring.GetUnitDefID(allMyUnits[i])].humanName
			if (isTrans == "Atlas") then 
				allTrans[ind] = allMyUnits[i]
				ind = ind+1
			end
		end
		
		if (allTrans == nil or #allTrans ==0) then
			return SUCCESS
		end

		
		return RUNNING	
	
	end
	return SUCCESS
end
