function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Unload at position",
		parameterDefs = {
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{
				name = "radiusOfPosition",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end

function Run(self, units, parameter)

	local radiusTU = parameter.radiusOfPosition
	local pos = parameter.position -- Vec3
	
	
	local myTeamID = Spring.GetMyTeamID()
	local allMyUnits = Spring.GetTeamUnits(myTeamID) 
	local counter = #allMyUnits
	
	local distToBase = 0
	
	
	
	
	
	for i = 1, counter do
		isTransporting = Spring.GetUnitIsTransporting(allMyUnits[i])
		
		if (isTransporting~=nil) --only for transport
		then  
			
			unitPos = Vec3(Spring.GetUnitPosition(allMyUnits[i]))
			distToBase = ((pos.x - unitPos.x)^2+(pos.y - unitPos.y)^2+(pos.z - unitPos.z)^2)^(0.5)
		
			if (#isTransporting>0 and distToBase<radiusTU) then --if transport is within a valid radius - unload
			
				local unitRadius = UnitDefs[Spring.GetUnitDefID(allMyUnits[i])].radius
				--local unitsInRange = 
				local unitCount = #Spring.GetUnitsInCylinder(unitPos.x, unitPos.z, unitRadius+30, myTeamID)
				
				if (unitsInRange~=nil)then 
					unitCount = #unitsInRange
				end
				
				if (unitCount<=2)then
					Spring.GiveOrderToUnit(allMyUnits[i],CMD.UNLOAD_UNITS,{unitPos.x, pos.y, unitPos.z, 50},{})
					--Spring.GiveOrderToUnit(allMyUnits[i],CMD.UNLOAD_UNITS,{pos.x, pos.y, pos.z, 0},{}) 					
					isUnloading = true
				else 
					local toX = pos.x + math.random(0-radiusTU,radiusTU)
					local toZ = pos.y + math.random(0-radiusTU,radiusTU)
					Spring.GiveOrderToUnit(allMyUnits[i],10, {toX,pos.y,toZ},{""})
					isUnloading = false
				end

				

			end
		end
		
	end
	
	if (isUnloading == false) then
		return SUCCESS
	end
	
	return RUNNING
end