local sensorInfo = {
	name = "GetPassengers",
	desc = "Get an array of units to transport",
	author = "NotaUserB",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- this sensor is not caching any values, it evaluates itself on every request

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(pos,radius)

	local myTeamID = Spring.GetMyTeamID()
	local allMyUnits = Spring.GetTeamUnits(myTeamID) 
	
	local Array = {}
	local ind = 1
	
	for i = 1, #allMyUnits do
		local cantBeTransported = UnitDefs[Spring.GetUnitDefID(allMyUnits[i])].cantBeTransported
		if (cantBeTransported == false) then 
			local unitPos = Vec3(Spring.GetUnitPosition(allMyUnits[i]))
			local distToBase = ((pos.x - unitPos.x)^2+(pos.y - unitPos.y)^2+(pos.z - unitPos.z)^2)^(0.5)
			if (distToBase>radius*2) then -- only units that are further than radius to the given position are accepted
				Array[ind] = allMyUnits[i]
				ind = ind+1
			end
		end
	end
	
	if (ind>1)then
		return Array
	else
		return nil
	end

end
